// server/server.js
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const port = 8090;
const fs = require('fs');

let comments = './comments.json'
let all_comments = require('./comments.json')

app.use(bodyParser.json());
app.use(cors());
app.use(express.urlencoded({ extended: true }));

let posts = [
    {
        id: 0,
        name: 'Albania',
        image: 'https://i.picsum.photos/id/1061/3264/2448.jpg?hmac=HoVLqUN6k0SMxCPSdgoc9_6Mhu3nGYm_lL3NgrDbwFU',
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
    },
    {
        id: 1,
        name: 'Turkey',
        description: `Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. 
            This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.32.10. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.`,
        image: 'https://i.picsum.photos/id/1018/3914/2935.jpg?hmac=3N43cQcvTE8NItexePvXvYBrAoGbRssNMpuvuWlwMKg'
    },
    {
        id: 2,
        name: 'Spain',
        description: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
        image: 'https://i.picsum.photos/id/1015/6000/4000.jpg?hmac=aHjb0fRa1t14DTIEBcoC12c5rAXOSwnVlaA5ujxPQ0I'
    },
    {
        id: 3,
        name: 'Italy',
        description: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.",
        image: 'https://i.picsum.photos/id/1036/4608/3072.jpg?hmac=Tn9CS_V7lFSMMgAI5k1M38Mdj-YEJR9dPJCyeXNpnZc'
    },
    {
        id: 4,
        name: 'Maldives',
        description: 'Nam et elit pretium, gravida enim non, vehicula nibh. Mauris dignissim nibh fermentum mauris gravida hendrerit. Aenean egestas, massa ut aliquet interdum, sem dui commodo risus, eget faucibus ipsum dolor vitae nisl. Aliquam semper, mauris sit amet mollis tempus, diam justo vestibulum arcu, vitae pretium nisi dui gravida urna. Proin vitae justo volutpat, dapibus massa ac, gravida justo. Praesent vitae imperdiet nisl. Donec lobortis quam id nisi sodales, vitae ultricies mauris suscipit. Vestibulum in mattis massa. Pellentesque vitae convallis justo. Suspendisse euismod justo luctus, porta lacus sed, elementum purus. Sed quis molestie nibh, et venenatis arcu. Ut pulvinar lacus eu lectus malesuada, et feugiat libero ultricies.',
        image: 'https://i.picsum.photos/id/1043/5184/3456.jpg?hmac=wsz2e0aFKEI0ij7mauIr2nFz2pzC8xNlgDHWHYi9qbc'
    },
    {
        id: 5,
        name: 'Colombia',
        description: 'Pellentesque accumsan metus a quam molestie, vel tristique ex feugiat. Aliquam non mollis tortor. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nam in metus posuere, facilisis massa eleifend, vestibulum urna. Fusce id enim erat. Vivamus rutrum tempor rutrum. Curabitur fermentum facilisis tortor ac vulputate. Pellentesque molestie, leo a aliquam interdum, mauris risus convallis purus, sed vehicula erat ipsum et felis. Morbi lectus tellus, elementum tempor semper et, porta quis turpis. Nam sollicitudin vulputate libero quis hendrerit. Etiam vehicula arcu eu nisi lobortis, vel sagittis diam vestibulum.',
        image: 'https://i.picsum.photos/id/1065/3744/5616.jpg?hmac=V64psST3xnjnVwmIogHI8krnL3edsh_sy4HNc3dJ_xY'
    },
    {
        id: 6,
        name: 'Miami',
        description: 'Praesent sollicitudin, ex id malesuada finibus, lacus diam tempor diam, quis vulputate eros mi sit amet nulla. Aliquam pellentesque ex sit amet massa lobortis, a tincidunt felis scelerisque. Morbi ac nisi urna. Morbi massa dui, hendrerit eget porta sit amet, viverra a orci. Vestibulum ligula massa, convallis at lectus ultrices, aliquet blandit ipsum. Donec ac imperdiet ligula. Proin porta molestie sem, quis posuere justo placerat vitae. Suspendisse non suscipit odio, ut posuere ex. Sed eget nunc in mi scelerisque varius. Nulla quis porta libero. Integer vitae arcu congue, fermentum ante eget, gravida magna. Curabitur eget lectus vitae ante tincidunt dictum tristique et mi. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nulla facilisi. Suspendisse efficitur vehicula arcu in consequat.',
        image: 'https://i.picsum.photos/id/13/2500/1667.jpg?hmac=SoX9UoHhN8HyklRA4A3vcCWJMVtiBXUg0W4ljWTor7s'
    },
    {
        id: 7,
        name: 'Dubai',
        description: 'Nullam molestie non purus eget venenatis. Nulla quis tellus est. Integer lobortis elit iaculis diam rutrum, id convallis turpis mollis. Duis sem odio, ullamcorper eu sollicitudin eu, pharetra ornare orci. Praesent fermentum ligula in ante accumsan interdum. Vivamus finibus, erat et faucibus facilisis, eros metus ultrices lectus, ac sollicitudin ante ex vitae felis. Fusce vel neque suscipit, lobortis nibh a, sagittis mi. Suspendisse ac maximus odio. Mauris vel lorem bibendum, feugiat magna nec, condimentum magna. Mauris rutrum leo orci, commodo malesuada quam condimentum id. Proin egestas sit amet enim sit amet aliquam. Integer laoreet, dolor eget iaculis imperdiet, eros massa cursus est, id blandit velit sapien vel ex. Cras id libero dolor. Mauris quis ornare metus, eu dictum risus. Morbi laoreet nec massa ut faucibus.',
        image: 'https://i.picsum.photos/id/182/2896/1944.jpg?hmac=lzw4TC7qF2R3BEJu05d0M6rdglY57ugjugCP6XoiMbQ'
    },
];

app.get('/posts', (req, res) => {
    res.send(posts);
});

app.post('/comments', (req, res) => {

    let new_comments = [...all_comments, req.body]

    if (fs.existsSync(comments)) {
        fs.unlink(comments, (error) => {
            fs.writeFile(comments, JSON.stringify(new_comments), error => {
                console.log('Comments error', error)
            })
        })
    } else {
        fs.writeFile(comments, JSON.stringify(new_comments), error => {
            console.log('Comments error', error)
        })
    }

    res.send(new_comments)
})

app.get('/comments', (req, res) => {
    res.send(all_comments)
})

app.get('/', (req, res) => {
  res.send(`Server is listening on port ${port}`)
});

// listen on the port
app.listen(port);
